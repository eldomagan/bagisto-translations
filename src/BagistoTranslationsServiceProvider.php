<?php

namespace EldoMagan\BagistoTranslations;

use Illuminate\Support\ServiceProvider;

class BagistoTranslationsServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/resources/lang' => resource_path('lang/vendor'),
            ]);

        }
    }
}