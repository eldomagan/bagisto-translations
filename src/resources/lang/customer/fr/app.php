<?php

return [
    'wishlist' => [
        'success' => 'Le produit a été ajouté avec succès à la liste de souhaits',
        'failure' => 'Le produit ne peut pas être ajouté à la liste de souhaits',
        'already' => 'Produit déjà présent dans votre liste de souhaits',
        'removed' => 'Le produit a été retiré avec succès de la liste de souhaits',
        'remove-fail' => 'Le produit ne peut pas être retiré de la liste de souhaits',
        'empty' => 'Vous n\'avez aucun produit dans votre liste de souhaits.',
        'select-options' => 'Nécessité de sélectionner les options avant d\'ajouter à la liste de souhaits',
        'remove-all-success' => 'Tous les articles de votre liste de souhaits ont été supprimés.',
    ],
    'reviews' => [
        'empty' => 'Vous n\'avez pas encore évalué un produit'
    ],
    'forget_password' => [
        'reset_link_sent' => 'Nous vous avons envoyé par e-mail le lien pour réinitialiser votre mot de passe.',
        'email_not_exist' => "Nous ne trouvons pas d'utilisateur avec cette adresse e-mail."
    ]
];