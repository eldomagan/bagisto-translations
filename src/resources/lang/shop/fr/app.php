<?php

return [
    'invalid_vat_format' => 'The given vat id has a wrong format',
    'security-warning' => 'Suspicious activity found!!!',
    'nothing-to-delete' => 'Nothing to delete',

    'layouts' => [
        'my-account' => 'Mon compte',
        'profile' => 'Profil',
        'address' => 'Adresses',
        'reviews' => 'Évaluations',
        'wishlist' => 'Liste de souhaits',
        'orders' => 'Commandes',
        'downloadable-products' => 'Produits téléchargeables'
    ],

    'common' => [
        'error' => 'Quelque chose s\'est mal passé, veuillez réessayer plus tard.',
        'no-result-found' => 'Nous n\'avons pu trouver aucun dossier.'
    ],

    'home' => [
        'page-title' => config('app.name') . ' - Acceuil',
        'featured-products' => 'Produits en vedette',
        'new-products' => 'Nouveaux produits',
        'verify-email' => 'Vérifier votre compte de messagerie',
        'resend-verify-email' => 'Renvoyer le courriel de vérification'
    ],

    'header' => [
        'title' => 'Mon Compte',
        'dropdown-text' => 'Gérer le panier, les commandes et la liste de souhaits',
        'sign-in' => 'Se connecter',
        'sign-up' => 'S\'inscrire',
        'account' => 'Mon compte',
        'cart' => 'Panier',
        'profile' => 'Profil',
        'wishlist' => 'Liste de souhaits',
        'cart' => 'Panier',
        'logout' => 'Déconnexion',
        'search-text' => 'Rechercher des produits ici'
    ],

    'minicart' => [
        'view-cart' => 'Afficher le panier d\'achat',
        'checkout' => 'Commander',
        'cart' => 'Panier',
        'zero' => '0'
    ],

    'footer' => [
        'subscribe-newsletter' => 'S\'abonner à la newsletter',
        'subscribe' => 'S\'abonner à newsletter',
        'locale' => 'Langue',
        'currency' => 'Devise',
    ],

    'subscription' => [
        'unsubscribe' => 'Se désabonner',
        'subscribe' => 'S\'abonner',
        'subscribed' => 'Vous êtes maintenant abonné à la newsletter',
        'not-subscribed' => 'Vous ne pouvez pas être inscrit à la newsletter, veuillez réessayer plus tard.',
        'already' => 'Vous êtes déjà abonné à la newsletter',
        'unsubscribed' => 'Vous êtes désinscrit de la newsletter.',
        'already-unsub' => 'Vous êtes déjà désabonné.',
        'not-subscribed' => 'Erreur ! Le mail ne peut être envoyé actuellement, veuillez réessayer plus tard.'
    ],

    'search' => [
        'no-results' => 'Aucun résultat trouvé',
        'page-title' => config('app.name') . ' - Recherche',
        'found-results' => 'Résultats de recherche trouvés',
        'found-result' => 'Résultat de la recherche trouvé',
        'analysed-keywords' => 'Mots-clés analysés'
    ],

    'reviews' => [
        'title' => 'Titre',
        'add-review-page-title' => 'Ajouter un avis',
        'write-review' => 'Rédiger un avis',
        'review-title' => 'Donnez un titre à votre évaluation',
        'product-review-page-title' => 'Évaluation du produit',
        'rating-reviews' => 'Notes et avis',
        'submit' => 'Soumettre',
        'delete-all' => 'Tous les avis ont été supprimés avec succès',
        'ratingreviews' => ':rating Notes & :review avis',
        'star' => 'Star',
        'percentage' => ':percentage %',
        'id-star' => 'star',
        'name' => 'Nom',
    ],

    'customer' => [
        'compare'           => [
            'text'                  => 'Comparer',
            'compare_similar_items' => 'Comparer les articles similaires',
            'add-tooltip'           => 'Ajouter le produit à la liste de comparaison',
            'added'                 => 'Le produit a été ajouté avec succès à la liste de comparaison',
            'already_added'         => 'Produit déjà ajouté à la liste de comparaison',
            'removed'               => 'Le produit a été retiré avec succès de la liste de comparaison',
            'removed-all'           => 'Tous les produits ont été supprimés avec succès de la liste de comparaison',
            'empty-text'            => "Vous n'avez pas de produits dans votre liste de comparaison.",
            'product_image'         => 'Image du produit',
            'actions'               => 'Actions',
        ],

        'signup-text' => [
            'account_exists' => 'Vous avez déjà un compte',
            'title' => 'Connectez-vous'
        ],

        'signup-form' => [
            'page-title' => 'Créer un nouveau compte client',
            'title' => 'S\'inscrire',
            'firstname' => 'Prénoms',
            'lastname' => 'Nom',
            'email' => 'Email',
            'password' => 'Mot de passe',
            'confirm_pass' => 'Confirmer le mot de passe',
            'button_title' => 'S\'inscrire',
            'agree' => 'Accepter',
            'terms' => 'Les conditions',
            'conditions' => 'Conditions',
            'using' => 'en utilisant ce site web',
            'agreement' => 'Accord',
            'success' => 'Compte créé avec succès.',
            'success-verify' => 'Compte créé avec succès, un e-mail de vérification vous a été envoyé.',
            'success-verify-email-unsent' => 'Le compte a été créé avec succès, mais l\'e-mail de vérification n\'a pas été envoyé.',
            'failed' => 'Erreur ! Impossible de créer votre compte, veuillez réessayer plus tard.',
            'already-verified' => 'Votre compte est déjà vérifié ou veuillez réessayer d\'envoyer un nouvel e-mail de vérification.',
            'verification-not-sent' => 'Erreur ! Problème dans lors de l\'envoi du mail de vérification, veuillez réessayer plus tard.',
            'verification-sent' => 'Mail de vérification envoyé',
            'verified' => 'Votre compte a été vérifié, essayez de vous connecter maintenant.',
            'verify-failed' => 'Nous ne pouvons pas vérifier votre compte de messagerie.',
            'dont-have-account' => 'Vous n\'avez pas de compte chez nous.',
            'customer-registration' => 'Client enregistré avec succès'
        ],

        'login-text' => [
            'no_account' => 'Vous n\'avez pas de compte',
            'title' => 'Inscrivez-vous',
        ],

        'login-form' => [
            'page-title' => 'Connexion client',
            'title' => 'Se connecter',
            'email' => 'Email',
            'password' => 'Mot de passe',
            'forgot_pass' => 'Mot de passe oublié?',
            'button_title' => 'Se connection',
            'remember' => 'Se souvenir de moi',
            'footer' => '© Copyright :year Webkul Software, Tous droits réservés',
            'invalid-creds' => 'Veuillez vérifier vos informations d\'identification et réessayer.',
            'verify-first' => 'Vérifiez d\'abord votre compte de messagerie.',
            'not-activated' => 'Votre activation demande l\'approbation de l\'administrateur',
            'resend-verification' => 'Renvoyer à nouveau le courrier de vérification'
        ],

        'forgot-password' => [
            'title' => 'Récupérer le mot de passe',
            'email' => 'Email',
            'submit' => 'Envoyer le mail de réinitialisation du mot de passe',
            'page_title' => 'Mot de passe oublié ?'
        ],

        'reset-password' => [
            'title' => 'Réinitialiser le mot de passe',
            'email' => 'Mail enregistré',
            'password' => 'Mot de passe',
            'confirm-password' => 'Confirmer le mot de passe',
            'back-link-title' => 'Retour à la page de connexion',
            'submit-btn-title' => 'Réinitialiser le mot de passe'
        ],

        'account' => [
            'dashboard' => 'Modifier le profil',
            'menu' => 'Menu',

            'general' => [
                'no' => 'Non',
                'yes' => 'Oui',
            ],

            'profile' => [
                'index' => [
                    'page-title' => 'Profil',
                    'title' => 'Profil',
                    'edit' => 'Modifier',
                ],

                'edit-success' => 'Le profil a été mis à jour avec succès.',
                'edit-fail' => 'Erreur ! Le profil ne peut être mis à jour, veuillez réessayer plus tard.',
                'unmatch' => 'L\'ancien mot de passe ne correspond pas.',

                'fname' => 'Prénom',
                'lname' => 'Nom',
                'gender' => 'Genre',
                'other' => 'Autre',
                'male' => 'Masculin',
                'female' => 'Féminin',
                'dob' => 'Date de naissance',
                'phone' => 'Téléphone',
                'email' => 'Email',
                'opassword' => 'Ancien mot de passe',
                'password' => 'Mot de passe',
                'cpassword' => 'Confirmer le mot de passe',
                'submit' => 'Mise à jour du profil',

                'edit-profile' => [
                    'title' => 'Modifier le profil',
                    'page-title' => 'Modifier le profil'
                ]
            ],

            'address' => [
                'index' => [
                    'page-title' => 'Adresse',
                    'title' => 'Adresse',
                    'add' => 'Ajouter une adresse',
                    'edit' => 'Modifier',
                    'empty' => 'Vous n\'avez pas d\'adresse enregistrée ici, essayez de la créer en cliquant sur le bouton d\'ajout.',
                    'create' => 'Créer une adresse',
                    'delete' => 'Supprimer',
                    'make-default' => 'Marque comme adresse par défaut',
                    'default' => 'Par défaut',
                    'contact' => 'Contact',
                    'confirm-delete' =>  'Voulez-vous vraiment supprimer cette adresse ?',
                    'default-delete' => 'L\'adresse par défaut ne peut pas être modifiée.',
                    'enter-password' => 'Entrez votre mot de passe.',
                ],

                'create' => [
                    'page-title' => 'Ajouter une adresse',
                    'company_name' => 'Nom de la société',
                    'first_name' => 'Prénom(s)',
                    'last_name' => 'Nom',
                    'vat_id' => 'Identifiant de TVA',
                    'vat_help_note' => '[Note : Utilisez le code pays avec l\'identifiant TVA. Par exemple, INV01234567891]',
                    'title' => 'Ajouter une adresse',
                    'street-address' => 'Adresse de la rue',
                    'country' => 'Pays',
                    'state' => 'État',
                    'select-state' => 'Sélectionnez une région, un état ou une province',
                    'city' => 'Ville',
                    'postcode' => 'Code postale',
                    'phone' => 'Téléphone',
                    'submit' => 'Enregistrer l\'adresse',
                    'success' => 'L\'adresse a été ajoutée avec succès.',
                    'error' => 'L\'adresse ne peut pas être ajoutée.'
                ],

                'edit' => [
                    'page-title' => 'Modifier l\'adresse',
                    'company_name' => 'Nom de la société',
                    'first_name' => 'Prénom(s)',
                    'last_name' => 'Nom',
                    'vat_id' => 'Identifiant TVA',
                    'title' => 'Modifier l\'adresse',
                    'street-address' => 'Adresse de la rue',
                    'submit' => 'Enregistrer l\'adresse',
                    'success' => 'L\'adresse a été mise à jour avec succès.',
                ],
                'delete' => [
                    'success' => 'Adresse supprimée avec succès',
                    'failure' => 'L\'adresse ne peut pas être supprimée',
                    'wrong-password' => 'Mot de passe incorrect !'
                ]
            ],

            'order' => [
                'index' => [
                    'page-title' => 'Commandes',
                    'title' => 'Commandes',
                    'order_id' => 'ID de la commande',
                    'date' => 'Date',
                    'status' => 'Statut',
                    'total' => 'Total',
                    'order_number' => 'N° de commande',
                    'processing' => 'En traitement',
                    'completed' => 'Complétée',
                    'canceled' => 'Annulée',
                    'closed' => 'Cloturée',
                    'pending' => 'En attente',
                    'pending-payment' => 'En attente de paiement',
                    'fraud' => 'Fraude'
                ],

                'view' => [
                    'page-tile' => 'Commande #:order_id',
                    'info' => 'Information',
                    'placed-on' => 'Date',
                    'products-ordered' => 'Produits commandés',
                    'invoices' => 'Factures',
                    'shipments' => 'Livraisons',
                    'SKU' => 'SKU',
                    'product-name' => 'Nom',
                    'qty' => 'Qté',
                    'item-status' => 'Statut de l\'article',
                    'item-ordered' => 'Commandées (:qty_ordered)',
                    'item-invoice' => 'Facturées (:qty_invoiced)',
                    'item-shipped' => 'Livrées (:qty_shipped)',
                    'item-canceled' => 'Annulées (:qty_canceled)',
                    'item-refunded' => 'Remboursées (:qty_refunded)',
                    'price' => 'Prix',
                    'total' => 'Total',
                    'subtotal' => 'Sous total',
                    'shipping-handling' => 'Livraison et manutention',
                    'tax' => 'Tax',
                    'discount' => 'Remise',
                    'tax-percent' => 'Pourcentage de taxe',
                    'tax-amount' => 'Montant des taxes',
                    'discount-amount' => 'Montant de la remise',
                    'grand-total' => 'Total général',
                    'total-paid' => 'Total payé',
                    'total-refunded' => 'Total remboursé',
                    'total-due' => 'Total dû',
                    'shipping-address' => 'Adresse de livraison',
                    'billing-address' => 'Adresse de facturation',
                    'shipping-method' => 'Méthode de livraison',
                    'payment-method' => 'Méthode de paiement',
                    'individual-invoice' => 'Facture #:invoice_id',
                    'individual-shipment' => 'Livraison #:shipment_id',
                    'print' => 'Imprimer',
                    'invoice-id' => 'ID Facture',
                    'order-id' => 'ID Commande',
                    'order-date' => 'Date commande',
                    'bill-to' => 'Facture à',
                    'ship-to' => 'Livré à',
                    'contact' => 'Contact',
                    'refunds' => 'Remboursements',
                    'individual-refund' => 'Remboursement #:refund_id',
                    'adjustment-refund' => 'Remboursement pour ajustement',
                    'adjustment-fee' => 'Frais d\'ajustement',
                    'cancel-btn-title' => 'Annuler',
                    'tracking-number' => 'Numéro de suivi',
                    'cancel-confirm-msg' => 'Êtes-vous sûr de vouloir annuler cette commande ?'
                ]
            ],

            'wishlist' => [
                'page-title' => 'Liste de souhaits',
                'title' => 'Liste de souhaits',
                'deleteall' => 'Tout supprimer',
                'moveall' => 'Mettre tous les produits dans le panier',
                'move-to-cart' => 'Déplacer vers le panier',
                'error' => 'Impossible d\'ajouter le produit à la liste de souhaits en raison de problèmes inconnus, veuillez revenir plus tard.',
                'add' => 'Produit ajouté avec succès à la liste de souhaits',
                'remove' => 'Produit retiré avec succès de la liste de souhaits',
                'add-wishlist-text'     => 'Ajouter le produit à la liste de souhaits',
                'remove-wishlist-text'  => 'Retirer le produit de la liste de souhaits',
                'moved' => 'Article déplacé avec succès dans le panier',
                'option-missing' => 'Les options du produit sont manquantes, le produit ne peut donc pas être déplacé vers la liste de souhaits.',
                'move-error' => 'Le produit ne peut pas être déplacé vers la liste de souhaits, veuillez réessayer plus tard.',
                'success' => 'Produit ajouté avec succès à la liste de souhaits',
                'failure' => 'Cet produit ne peut pas être ajouté à la liste de souhaits, veuillez réessayer plus tard.',
                'already' => 'Produit déjà présent dans votre liste de souhaits',
                'removed' => 'Produit retiré avec succès de la liste de souhaits',
                'remove-fail' => 'Le produit ne peut pas être retiré de la liste de souhaits, veuillez réessayer plus tard.',
                'empty' => 'Vous n\'avez aucun produit dans votre liste de souhaits.',
                'remove-all-success' => 'Tous les produits de votre liste de souhaits ont été supprimés.',
            ],

            'downloadable_products' => [
                'title' => 'Produits téléchargeables',
                'order-id' => 'ID Commande',
                'date' => 'Date',
                'name' => 'Titre',
                'status' => 'Statut',
                'pending' => 'En attente',
                'available' => 'Disponible',
                'expired' => 'Expiré',
                'remaining-downloads' => 'Téléchargements restants',
                'unlimited' => 'Illimité',
                'download-error' => 'Le lien de téléchargement a expiré.'
            ],

            'review' => [
                'index' => [
                    'title' => 'Avis',
                    'page-title' => 'Avis'
                ],

                'view' => [
                    'page-tile' => 'Avis #:id',
                ]
            ]
        ]
    ],

    'products' => [
        'layered-nav-title' => 'Acheté par',
        'price-label' => 'Aussi bas que',
        'remove-filter-link-title' => 'Effacer tout',
        'filter-to' => 'À',
        'sort-by' => 'Trier par',
        'from-a-z' => 'De A à Z',
        'from-z-a' => 'De Z à A',
        'newest-first' => 'Le plus récent en premier',
        'oldest-first' => 'Le plus ancien en premier',
        'cheapest-first' => 'Le moins cher en premier',
        'expensive-first' => 'Le plus cher en premier',
        'show' => 'Voir',
        'pager-info' => ':showing articles sur :total',
        'description' => 'Description',
        'specification' => 'Spécifications',
        'total-reviews' => ':total avis',
        'total-rating' => ':total_rating notes & :total_reviews avis',
        'by' => 'Par :name',
        'up-sell-title' => 'Nous avons trouvé d\'autres produits qui pourraient vous plaire !',
        'related-product-title' => 'Produits associés',
        'cross-sell-title' => 'Plus de choix',
        'reviews-title' => 'Notes & Avis',
        'write-review-btn' => 'Rediger un avis',
        'choose-option' => 'Choisissez une option',
        'sale' => 'En promotion',
        'new' => 'Nouveau',
        'empty' => 'Aucun produit disponible dans cette catégorie',
        'add-to-cart' => 'Ajouter au panier',
        'book-now' => 'Réservez maintenant',
        'buy-now' => 'Acheter maintenant',
        'whoops' => 'Oups !',
        'quantity' => 'Quantité',
        'in-stock' => 'En stock',
        'out-of-stock' => 'En rupture de stock',
        'view-all' => 'Voir tout',
        'select-above-options' => 'Veuillez d\'abord sélectionner les options ci-dessus.',
        'less-quantity' => 'La quantité ne peut être inférieure à un.',
        'samples' => 'Échantillons',
        'links' => 'Liens',
        'sample' => 'Échantillon',
        'name' => 'Nom',
        'qty' => 'Qté',
        'starting-at' => 'À partir de',
        'customize-options' => 'Personnaliser les options',
        'choose-selection' => 'Choisissez une sélection',
        'your-customization' => 'Votre personnalisation',
        'total-amount' => 'Montant total',
        'none' => 'Aucun',
        'available-for-order' => 'Disponible pour commande',
        'settings' => 'Paramètres',
        'compare_options' => 'Comparer les options',
    ],

    // 'reviews' => [
    //     'empty' => 'You Have Not Reviewed Any Of Product Yet'
    // ]

    'buynow' => [
        'no-options' => 'Veuillez sélectionner les options avant d\'acheter ce produit.'
    ],

    'checkout' => [
        'cart' => [
            'integrity' => [
                'missing_fields' => 'Certains champs obligatoires manquent pour ce produit.',
                'missing_options' => 'Les options manquent pour ce produit.',
                'missing_links' => 'Les liens de téléchargement sont absents pour ce produit.',
                'qty_missing' => 'Au moins un produit doit avoir plus d\'une quantité.',
                'qty_impossible' => 'Impossible d\'ajouter plus d\'un de ces produits au panier.'
            ],
            'create-error' => 'Un problème est survenu lors de la création d\'une instance de panier.',
            'title' => 'Panier d\'achat',
            'empty' => 'Votre panier est vide',
            'update-cart' => 'Mise à jour du panier',
            'continue-shopping' => 'Continuer les achats',
            'proceed-to-checkout' => 'Procéder au paiement',
            'remove' => 'Supprimer',
            'remove-link' => 'Supprimer',
            'move-to-wishlist' => 'Déplacer vers la liste de souhaits',
            'move-to-wishlist-success' => 'Le produit a été déplacé vers la liste de souhaits avec succès.',
            'move-to-wishlist-error' => 'Impossible de déplacer le produit vers la liste de souhaits, veuillez réessayer plus tard.',
            'add-config-warning' => 'Veuillez sélectionner l\'option avant d\'ajouter au panier.',
            'quantity' => [
                'quantity' => 'Quantité',
                'success' => 'Le(s) article(s) du panier ont été mis à jour avec succès.',
                'illegal' => 'La quantité ne peut être inférieure à un.',
                'inventory_warning' => 'La quantité demandée n\'est pas disponible, veuillez réessayer plus tard.',
                'error' => 'Impossible de mettre à jour le(s) article(s) pour le moment, veuillez réessayer plus tard.'
            ],

            'item' => [
                'error_remove' => 'Aucun produit à retirer du panier.',
                'success' => 'Le produit a été ajouté au panier avec succès.',
                'success-remove' => 'Le produit a été retiré du panier avec succès.',
                'error-add' => 'Le produit ne peut être ajouté au panier, veuillez réessayer plus tard.',
                'inactive' => 'Un produit est inactif et a été retiré du panier.',
                'inactive-add' => 'Un produit inactif ne peut pas être ajouté au panier.',
            ],
            'quantity-error' => 'La quantité demandée n\'est pas disponible.',
            'cart-subtotal' => 'Sous-total du panier',
            'cart-remove-action' => 'Voulez-vous vraiment effectuer cette action ?',
            'partial-cart-update' => 'Seuls certains des produits ont été mis à jour.',
            'link-missing' => '',
            'event' => [
                'expired' => 'Cet événement a expiré.'
            ]
        ],

        'onepage' => [
            'title' => 'Commander',
            'information' => 'Information',
            'shipping' => 'Livraison',
            'payment' => 'Paiement',
            'complete' => 'Terminer',
            'review' => 'Évaluation',
            'billing-address' => 'Adresse de facturation',
            'sign-in' => 'Se connecter',
            'company-name' => 'Nom de la société',
            'first-name' => 'Prénom',
            'last-name' => 'Nom',
            'email' => 'E-mail',
            'address1' => 'Adresse de la rue',
            'city' => 'Ville',
            'state' => 'État',
            'select-state' => 'Sélectionnez une région, un état ou une province',
            'postcode' => 'Code postal',
            'phone' => 'Téléphone',
            'country' => 'Pays',
            'order-summary' => 'Récapitulatif de la commande',
            'shipping-address' => 'Adresse de livraison',
            'use_for_shipping' => 'Livrer à cette adresse',
            'continue' => 'Continuer',
            'shipping-method' => 'Sélectionnez la méthode de livraison',
            'payment-methods' => 'Sélectionnez la méthode de paiement',
            'payment-method' => 'Méthode de paiement',
            'summary' => 'Récapitulatif de la commande',
            'price' => 'Prix',
            'quantity' => 'Quantité',
            'billing-address' => 'Adresse de facturation',
            'shipping-address' => 'Adresse de livraison',
            'contact' => 'Contact',
            'place-order' => 'Passer la commande',
            'new-address' => 'Ajouter une nouvelle adresse',
            'save_as_address' => 'Enregistrer cette adresse',
            'apply-coupon' => 'Appliquer un coupon',
            'amt-payable' => 'Montant à payer',
            'got' => 'Got',
            'free' => 'Gratuit',
            'coupon-used' => 'Coupon utilisé',
            'applied' => 'Appliqué',
            'back' => 'Précédent',
            'cash-desc' => 'Paiement à la livraison',
            'money-desc' => 'Transfert d\'argent',
            'paypal-desc' => 'Paypal Standard',
            'free-desc' => 'Il s\'agit d\'une livraison gratuite',
            'flat-desc' => 'Il s\'agit d\'un taux fixe',
            'password' => 'Mot de passe',
            'login-exist-message' => 'Vous avez déjà un compte chez nous, connectez-vous ou continuez en tant qu\'invité.',
            'enter-coupon-code' => 'Entrez le code de coupon'
        ],

        'total' => [
            'order-summary' => 'Récapitulatif de la commande',
            'sub-total' => 'Produits',
            'grand-total' => 'Total général',
            'delivery-charges' => 'Frais de livraison',
            'tax' => 'Taxe',
            'discount' => 'Réduction',
            'price' => 'prix',
            'disc-amount' => 'Montant de la réduction',
            'new-grand-total' => 'Nouveau Total général',
            'coupon' => 'Code coupon',
            'coupon-applied' => 'Coupon appliqué',
            'remove-coupon' => 'Retirer le coupon',
            'cannot-apply-coupon' => 'Impossible d\'appliquer le coupon',
            'invalid-coupon' => 'Le code coupon n\'est pas valide.',
            'success-coupon' => 'Le code promotionnel a été appliqué avec succès.',
            'coupon-apply-issue' => 'Le code promo ne peut pas être appliqué.'
        ],

        'success' => [
            'title' => 'Commande passée avec succès',
            'thanks' => 'Merci pour votre commande !',
            'order-id-info' => 'Votre numéro de commande est #:order_id',
            'info' => 'Nous vous enverrons par e-mail les détails de votre commande et les informations de suivi.'
        ]
    ],

    'mail' => [
        'order' => [
            'subject' => 'Confirmation de la nouvelle commande',
            'heading' => 'Confirmation de la commande !',
            'dear' => 'Cher :customer_name',
            'dear-admin' => 'Cher :admin_name',
            'greeting' => 'Merci pour votre commande :order_id passée le :created_at',
            'greeting-admin' => 'Commande :order_id reçu le :created_at',
            'summary' => 'Récapitulatif de la commande',
            'shipping-address' => 'Adresse de livraison',
            'billing-address' => 'Adresse de facturation',
            'contact' => 'Contact',
            'shipping' => 'Méthode de livraison',
            'payment' => 'Méthode de paiement',
            'price' => 'Prix',
            'quantity' => 'Quantité',
            'subtotal' => 'Sous-total',
            'shipping-handling' => 'Livraison et manutention',
            'tax' => 'Taxe',
            'discount' => 'Réduction',
            'grand-total' => 'Total général',
            'final-summary' => 'Merci de l\'intérêt que vous portez à notre boutique. Nous vous enverrons le numéro de suivi une fois l\'envoi effectué.',
            'help' => 'Si vous avez besoin d\'aide, veuillez nous contacter à l\'adresse :support_email',
            'thanks' => 'Merci !',

            'comment' => [
                'subject' => 'Nouveau commentaire ajouté à votre commande #:order_id',
                'dear' => 'Cher :customer_name',
                'final-summary' => 'Merci de l\'intérêt que vous portez à notre boutique',
                'help' => 'Si vous avez besoin d\'aide, veuillez nous contacter à l\'adresse :support_email',
                'thanks' => 'Merci !',
            ],

            'cancel' => [
                'subject' => 'Confirmation de l\'annulation de la commande',
                'heading' => 'Commande annulée',
                'dear' => 'Cher :customer_name',
                'greeting' => 'Votre commande n° :order_id  passée le :created_at a été annulée.',
                'summary' => 'Récapitulatif de la commande',
                'shipping-address' => 'Adresse de livraison',
                'billing-address' => 'Adresse de facturation',
                'contact' => 'Contact',
                'shipping' => 'Méthode de livraison',
                'payment' => 'Méthode de paiement',
                'subtotal' => 'Sous-total',
                'shipping-handling' => 'Livraison et manutention',
                'tax' => 'Taxe',
                'discount' => 'Réduction',
                'grand-total' => 'Total général',
                'final-summary' => 'Merci de l\'intérêt que vous portez à notre boutique',
                'help' => 'Si vous avez besoin d\'aide, veuillez nous contacter à l\'adresse :support_email',
                'thanks' => 'Merci !',
            ]
        ],

        'invoice' => [
            'heading' => 'Votre facture #:invoice_id pour la commande #:order_id',
            'subject' => 'Facture pour votre commande #:order_id',
            'summary' => 'Récapitulatif de la facture',
        ],

        'shipment' => [
            'heading' => 'La livraison #:shipment_id a été générée pour la commande #:order_id',
            'inventory-heading' => 'La nouvelle livraison #:shipment_id a été générée pour la commande #:order_id',
            'subject' => 'Livraison de votre commande #:order_id',
            'inventory-subject' => 'Une nouvelle livraison a été générée pour la commande #:order_id',
            'summary' => 'Récapitulatif de la livraison',
            'carrier' => 'Transporteur',
            'tracking-number' => 'Numéro de suivi',
            'greeting' => 'Une commande :order_id a été passée le :created_at',
        ],

        'refund' => [
            'heading' => 'Votre remboursement #:refund_id pour la commande #:order_id',
            'subject' => 'Remboursement de votre commande #:order_id',
            'summary' => 'Récapitulatif du remboursement',
            'adjustment-refund' => 'Remboursement de l\'ajustement',
            'adjustment-fee' => 'Frais d\'ajustement'
        ],

        'forget-password' => [
            'subject' => 'Réinitialisation du mot de passe du client',
            'dear' => 'Cher :name',
            'info' => 'Vous recevez cet e-mail car nous avons reçu une demande de réinitialisation du mot de passe de votre compte.',
            'reset-password' => 'Réinitialiser le mot de passe',
            'final-summary' => 'Si vous n\'avez pas demandé la réinitialisation de votre mot de passe, aucune action supplémentaire n\'est requise.',
            'thanks' => 'Merci !'
        ],

        'update-password' => [
            'subject' => 'Mise à jour du mot de passe',
            'dear' => 'Cher :name',
            'info' => 'Vous recevez cet e-mail car vous avez mis à jour votre mot de passe.',
            'thanks' => 'Merci !'
        ],

        'customer' => [
            'new' => [
                'dear' => 'Bonjour :customer_name',
                'username-email' => 'Nom d\'utilisateur/e-mail',
                'subject' => 'Enregistrement d\'un nouveau client',
                'password' => 'Mot de passe',
                'summary' => 'Your account has been created.
                Les détails de votre compte sont ci-dessous : ',
                'thanks' => 'Merci !',
            ],

            'registration' => [
                'subject' => 'Enregistrement d\'un nouveau client',
                'customer-registration' => 'Client enregistré avec succès',
                'dear' => 'Bonjour :customer_name',
                'greeting' => 'Bienvenue et merci de vous inscrire chez nous !',
                'summary' => 'Votre compte a été créé avec succès et vous pouvez vous connecter en utilisant votre adresse e-mail et votre mot de passe. En vous connectant, vous pourrez accéder à d\'autres services, notamment consulter vos commandes passées, vos listes de souhaits et modifier les informations de votre compte.',
                'thanks' => 'Merci !',
            ],

            'verification' => [
                'heading' => config('app.name') . ' - Vérification de l\'e-mail',
                'subject' => 'Vérification de l\'e-mail',
                'verify' => 'Vérifier votre compte',
                'summary' => 'Il s\'agit du mail permettant de vérifier que l\'adresse électronique que vous avez saisie est bien la vôtre.
                Veuillez cliquer sur le bouton Vérifier votre compte ci-dessous pour vérifier votre compte.'
            ],

            'subscription' => [
                'subject' => 'Email d\'abonnement',
                'greeting' => ' Bienvenue à ' . config('app.name') . ' - Email d\'abonnement',
                'unsubscribe' => 'Se désabonner',
                'summary' => 'Merci de me mettre dans votre boîte de réception. Cela fait un moment que tu n\'as pas lu les mail de ' . config('app.name') . ' et nous ne voulons pas surcharger votre boîte de réception. Si vous ne souhaitez toujours pas recevoir
                les dernières nouvelles sur le marketing par mail, alors cliquez sur le bouton ci-dessous.'
            ]
        ]
    ],

    'webkul' => [
        'copy-right' => '© Copyright :year Webkul Software, Tous droits réservés',
    ],

    'response' => [
        'create-success' => ':name créé avec succès.',
        'update-success' => ':name mis à jour avec succès.',
        'delete-success' => ':name supprimé avec succès.',
        'submit-success' => ':name soumis avec succès.'
    ],
];