<?php

return [
    'admin' => [
        'system' => [
            'social-login' => 'Connexion avec réseaux sociaux',
            'enable-facebook' => 'Activer Facebook',
            'enable-twitter' => 'Activer Twitter',
            'enable-google' => 'Activer Google',
            'enable-twitter' => 'Activer Twitter',
            'enable-linkedin' => 'Activer LinkedIn',
            'enable-github' => 'Activer Github'
        ]
    ],

    'shop' => [
        'customer' => [
            'login-form' => [
                'continue-with-facebook' => 'Continuer avec Facebook',
                'continue-with-twitter' => 'Continuer avec Twitter',
                'continue-with-google' => 'Continuer avec Google',
                'continue-with-linkedin' => 'Continuer avec LinkedIn',
                'continue-with-github' => 'Continuer avec Github',
                'or' => 'Ou'
            ]
        ]
    ]
];