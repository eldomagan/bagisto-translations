<?php
return [
    'datagrid' => [
        'actions' => 'Actions',
        'id' => 'Les colonnes d\'index ne peuvent avoir q\'une valeur supérieure à zéro',

        'massaction' => [
            'mass-delete-confirm' => 'Voulez-vous vraiment supprimer ces :resource sélectionné(e)s ?',
            'mass-update-status' => 'Voulez-vous vraiment mettre à jour le statut de ces :resource sélectionné(e)s ?',
            'delete' => 'Voulez-vous vraiment effectuer cette action ?',
            'edit' => 'Voulez-vous vraiment modifier :resource ?',
        ],

        'zero-index' => 'Les colonnes d\'index ne peuvent avoir q\'une valeur supérieure à zéro',
        'no-records' => 'Aucun élément trouvé',
        'filter-fields-missing' => 'Certains des champs obligatoires sont nuls, veuillez vérifier la colonne, la condition et la valeur correctement.',
        'click_on_action' => 'Voulez-vous vraiment effectuer cette action ?',
        'search' => 'Rechercher ici...',
        'filter' => 'Filtrer',
        'column' => 'Sélectionner la colonne',
        'condition' => 'Sélectionner la condition',
        'contains' => 'Contient',
        'ncontains' => 'Ne contient pas',
        'equals' => 'Est égal à',
        'nequals' => 'N\'est pas égal à',
        'greater' => 'Supérieur à',
        'less' => 'Inférieur à',
        'greatere' => 'Supérieur ou égal à',
        'lesse' => 'Inférieur ou égal à',
        'value' => 'Sélectionnez une valeur',
        'true' => 'Vrai / Actif',
        'false' => 'Faux / Inactif',
        'between' => 'Est entre',
        'apply' => 'Appliquer',
        'items-per-page' => 'Eléments par page',
        'value-here' => 'Valeur ici',
        'numeric-value-here' => 'Valeur numérique ici',
        'submit' => 'Soumettre'
    ]
];